﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace EDColorChanger.Core
{
    public class ColorChanger
    {
        /// <summary>
        /// Changes the color the graphics config file using the matrix supplied. Optionally,
        /// creates a backup.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="matrix">The matrix.</param>
        /// <param name="backupFileName">Name of the backup file.</param>
        /// <param name="overwriteBackup">if set to <c>true</c> [overwrite backup].</param>
        public static void ChangeColor(string filename, EdColorMatrix matrix, string backupFileName = null, bool overwriteBackup = false)
        {
            // See if they want to do a backup First, see if we have a path, then if we want to
            // overwrite it. If there's nothing to overwrite, we're good
            if (!string.IsNullOrWhiteSpace(backupFileName) && (!File.Exists(backupFileName) || overwriteBackup))
            {
                // Make a backup of the file
                File.Copy(filename, backupFileName, overwrite: overwriteBackup);
            }

            // Make sure the file exists
            if (!File.Exists(filename))
            {
                return;
            }

            // Get the XML document
            var xml = OpenXmlFile(filename);

            if (xml == null)
            {
                return;
            }

            // Get the base node
            var baseNode = xml.DocumentElement;

            // If we can't find that, we have no business being here. Run away!
            if (baseNode == null)
            {
                return;
            }

            // Find the XML
            var redMatrixNode = GetRedMatrixNode(baseNode);
            var greenMatrixNode = GetGreenMatrixNode(baseNode);
            var blueMatrixNode = GetBlueMatrixNode(baseNode);

            // Change the XML
            redMatrixNode.InnerText = matrix.GetRedLine();
            greenMatrixNode.InnerText = matrix.GetGreenLine();
            blueMatrixNode.InnerText = matrix.GetBlueLine();

            // Write the file out
            using (var file_out = File.CreateText(filename))
            {
                // Set so that we indent and pretty print, so that the user's eyes don't bleed if
                // they check the file themselves
                var settings = new XmlWriterSettings
                {
                    OmitXmlDeclaration = false,
                    Indent = true,
                    NewLineOnAttributes = true
                };

                xml.Save(XmlWriter.Create(file_out, settings));
            }
        }

        /// <summary>
        /// Reads the color matrix from the graphicsConfiguration file.
        /// </summary>
        /// <param name="filename">The filename of the graphics configuration XML file.</param>
        /// <returns></returns>
        /// <exception cref="System.IO.FileNotFoundException">XML file not found</exception>
        /// <exception cref="System.Xml.XmlException">Cannot find base node!</exception>
        public static EdColorMatrix ReadColorMatrix(string filename)
        {
            // Get the XML document
            var xml = OpenXmlFile(filename);

            // Make sure you can find it
            if (xml == null)
            {
                throw new FileNotFoundException("XML file not found", filename);
            }

            // Get the base node
            var baseNode = xml.DocumentElement;

            if (baseNode == null)
            {
                throw new XmlException("Cannot find base node!");
            }

            // Get the values and create a matrix out of them
            var redMatrixText = GetRedMatrixNode(baseNode).InnerText;
            var greenMatrixText = GetGreenMatrixNode(baseNode).InnerText;
            var blueMatrixText = GetBlueMatrixNode(baseNode).InnerText;

            var m = EdColorMatrix.CreateFromMatrixString(redMatrixText, greenMatrixText, blueMatrixText);

            return m;
        }

        /// <summary>
        /// Gets the blue matrix node. Based on the MatrixBlue tag
        /// </summary>
        /// <param name="baseNode">The base node.</param>
        /// <returns>The matrix blue Node.</returns>
        private static XmlNode GetBlueMatrixNode(XmlElement baseNode)
        {
            return baseNode.SelectSingleNode(@"//GUIColour/Default/MatrixBlue");
        }

        /// <summary>
        /// Gets the green matrix node. Based on the MatrixGreen tag
        /// </summary>
        /// <param name="baseNode">The base node.</param>
        /// <returns>The matrix green Node.</returns>
        private static XmlNode GetGreenMatrixNode(XmlElement baseNode)
        {
            return baseNode.SelectSingleNode(@"//GUIColour/Default/MatrixGreen");
        }

        /// <summary>
        /// Gets the red matrix node. Based on the MatrixRed tag
        /// </summary>
        /// <param name="baseNode">The base node.</param>
        /// <returns>The matrix red Node.</returns>
        private static XmlNode GetRedMatrixNode(XmlElement baseNode)
        {
            return baseNode.SelectSingleNode(@"//GUIColour/Default/MatrixRed");
        }

        /// <summary>
        /// Opens the XML file. Reads the content and then closes the file
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>An XML document representing the contents of the file.</returns>
        private static XmlDocument OpenXmlFile(string filename)
        {
            XmlTextReader file = null;
            XmlDocument xml = null;

            try
            {
                // Open the file
                file = new XmlTextReader(filename);
                xml = new XmlDocument();

                xml.Load(file);
            }
            finally
            {
                if (file != null && file.ReadState != ReadState.Closed)
                {
                    // Close the files
                    file.Close();
                }
            }
            return xml;
        }
    }
}