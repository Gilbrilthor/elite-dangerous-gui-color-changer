using System;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;

namespace EDColorChanger.Core
{
    /// <summary>
    /// A color matrix as it pertains to Elite: Dangerous
    /// </summary>
    public class EdColorMatrix : IEquatable<EdColorMatrix>
    {
        /// <summary>
        /// The decimal places to round for equality
        /// </summary>
        private const int DecimalPlacesToRoundForEquality = 2;

        /// <summary>
        /// The culture to use for parsing
        /// </summary>
        static private readonly CultureInfo Culture = new CultureInfo("en-US");

        /// <summary>
        /// Initializes a new instance of the <see cref="EdColorMatrix"/> class.
        /// </summary>
        public EdColorMatrix()
        {
            Matrix = new decimal[3, 3];
        }

        /// <summary>
        /// Gets or sets the name of the author.
        /// </summary>
        /// <value>The name of the author.</value>
        public string AuthorName { get; set; }

        /// <summary>
        /// Gets or sets the matrix.
        /// </summary>
        /// <value>
        /// The matrix. First number is X coord (left = 0). Second number is Y coord (top = 0).
        /// </value>
        public decimal[,] Matrix { get; set; }

        /// <summary>
        /// Gets or sets the name of the scheme.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the reference pic URL.
        /// </summary>
        /// <value>The reference pic URL.</value>
        public string ReferencePicURL { get; set; }

        /// <summary>
        /// Builds from a scheme line.
        /// </summary>
        /// <param name="schemeLine">The scheme line.</param>
        /// <returns>the Matrix that is represented by the scheme line</returns>
        public static EdColorMatrix BuildFromSchemeLine(string schemeLine)
        {
            var m = new EdColorMatrix();
            // Format is Name|Author|r1|r2|r3|g1|g2|g3|b1|b2|b3|refPicDirectLink split line
            var stringArray = schemeLine.Split('|');
            // pull description fields
            m.Name = stringArray[0];
            m.AuthorName = stringArray[1];
            m.ReferencePicURL = stringArray.Length > 11 ? stringArray[11] : null;

            // pull matrix
            m.Matrix = new decimal[3, 3];

            try
            {
                m.Matrix[0, 0] = decimal.Parse(stringArray[2], Culture);
                m.Matrix[1, 0] = decimal.Parse(stringArray[3], Culture);
                m.Matrix[2, 0] = decimal.Parse(stringArray[4], Culture);
                m.Matrix[0, 1] = decimal.Parse(stringArray[5], Culture);
                m.Matrix[1, 1] = decimal.Parse(stringArray[6], Culture);
                m.Matrix[2, 1] = decimal.Parse(stringArray[7], Culture);
                m.Matrix[0, 2] = decimal.Parse(stringArray[8], Culture);
                m.Matrix[1, 2] = decimal.Parse(stringArray[9], Culture);
                m.Matrix[2, 2] = decimal.Parse(stringArray[10], Culture);
            }
            catch (FormatException fe)
            {
                var e = new FormatException(string.Format("{0} has invalid decimal", m.Name));
                e.Data.Add("MatrixName", m.Name);
                throw e;
            }

            return m;
        }

        /// <summary>
        /// Creates a matrix from the XML config file's matrix strings.
        /// </summary>
        /// <param name="redMatrixString">The red matrix string.</param>
        /// <param name="greenMatrixString">The green matrix string.</param>
        /// <param name="blueMatrixString">The blue matrix string.</param>
        /// <returns>The matrix represented by the 3 matrix strings.</returns>
        public static EdColorMatrix CreateFromMatrixString(string redMatrixString, string greenMatrixString,
            string blueMatrixString)
        {
            // Get the 9 values
            var redStringArray = redMatrixString.Trim().Split(',');
            var greenStringArray = greenMatrixString.Trim().Split(',');
            var blueStringArray = blueMatrixString.Trim().Split(',');

            // Find out the max lengths
            var maxLength = new[]
            {
                redStringArray.Length,
                greenStringArray.Length,
                blueStringArray.Length
            }.Max();

            // Get the decimal values from the 9 strings
            var numberArrays = new[]
            {
                (from s in redStringArray select decimal.Parse(s, Culture)).ToArray(),
                (from s in greenStringArray select decimal.Parse(s, Culture)).ToArray(),
                (from s in blueStringArray select decimal.Parse(s, Culture)).ToArray()
            };

            // Get the sum of the values
            var sums = new[]
            {
                numberArrays[0].Sum(),
                numberArrays[1].Sum(),
                numberArrays[2].Sum(),
            };

            var m = new EdColorMatrix { Matrix = new decimal[maxLength, 3] };

            for (int y = 0; y < m.Matrix.GetLength(0); y++)
            {
                for (int x = 0; x < m.Matrix.GetLength(1); x++)
                {
                    // TODO: Fix clamping so that colors don't oversaturate
                    m.Matrix[x, y] = ((decimal)numberArrays[y][x]); // / (sums[y] > 1 ? sums[y] : 1);
                }
            }

            return m;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type. This
        /// is based on their values
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter;
        /// otherwise, false.
        /// </returns>
        public bool Equals(EdColorMatrix other)
        {
            var mat1Values = Matrix.Cast<decimal>().ToList();
            var mat2Values = other.Matrix.Cast<decimal>().ToList();

            if (mat1Values.Count != mat2Values.Count)
            {
                return false;
            }

            for (int i = 0; i < mat1Values.Count && i < mat2Values.Count; i++)
            {
                if (decimal.Round(mat1Values[i], DecimalPlacesToRoundForEquality)
                    != decimal.Round(mat2Values[i], DecimalPlacesToRoundForEquality))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance;
        /// otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var otherMatrix = obj as EdColorMatrix;

            if (otherMatrix == null)
            {
                return false;
            }

            return this.Equals(otherMatrix);
        }

        /// <summary>
        /// Gets the blue line.
        /// </summary>
        /// <returns>The line to use for the blue matrix.</returns>
        public string GetBlueLine()
        {
            return string.Format("{0}, {1}, {2}", Matrix[0, 2].ToString(Culture),
                Matrix[1, 2].ToString(Culture),
                Matrix[2, 2].ToString(Culture));
        }

        /// <summary>
        /// Gets the color matrix.
        /// </summary>
        /// <returns>a ColorMatrix represented by this EdColorMatrix</returns>
        public ColorMatrix GetColorMatrix()
        {
            var cm = new ColorMatrix();

            for (int y = 0; y < Matrix.GetLength(0); y++)
            {
                for (int x = 0; x < Matrix.GetLength(1); x++)
                {
                    cm[x, y] = (float)Matrix[x, y];
                }
            }

            cm[3, 3] = 1;
            cm[4, 4] = 1;

            return cm;
        }

        /// <summary>
        /// Gets a deep copy of the Color matrix.
        /// </summary>
        /// <returns></returns>
        public EdColorMatrix GetCopy()
        {
            var m = new EdColorMatrix { Name = Name, AuthorName = AuthorName, ReferencePicURL = ReferencePicURL };

            Array.Copy(Matrix, 0, m.Matrix, 0, Matrix.Length);

            return m;
        }

        /// <summary>
        /// Gets the green line.
        /// </summary>
        /// <returns>XML inner text of the green line</returns>
        public string GetGreenLine()
        {
            return string.Format("{0}, {1}, {2}", Matrix[0, 1].ToString(Culture),
                Matrix[1, 1].ToString(Culture),
                Matrix[2, 1].ToString(Culture));
        }

        /// <summary>
        /// Gets the red line.
        /// </summary>
        /// <returns>XML inner text of the red line</returns>
        public string GetRedLine()
        {
            return string.Format("{0}, {1}, {2}", Matrix[0, 0].ToString(Culture),
                Matrix[1, 0].ToString(Culture),
                Matrix[2, 0].ToString(Culture));
        }

        /// <summary>
        /// Gets the scheme line representing the matrix.
        /// </summary>
        /// <returns>a string representing the matrix</returns>
        public string GetSchemeLine()
        {
            return string.Format(
                "{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}"
                , Name
                , AuthorName
                , Matrix[0, 0].ToString(Culture)
                , Matrix[1, 0].ToString(Culture)
                , Matrix[2, 0].ToString(Culture)
                , Matrix[0, 1].ToString(Culture)
                , Matrix[1, 1].ToString(Culture)
                , Matrix[2, 1].ToString(Culture)
                , Matrix[0, 2].ToString(Culture)
                , Matrix[1, 2].ToString(Culture)
                , Matrix[2, 2].ToString(Culture)
                , ReferencePicURL
                );
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance. Used for testing
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents this instance.</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendFormat("{0} ({1})\n", Name, AuthorName);

            for (var y = 0; y < this.Matrix.GetLength(0); y++)
            {
                for (var x = 0; x < this.Matrix.GetLength(1); x++)
                {
                    var numberString = Matrix[x, y].ToString(Culture);
                    sb.AppendFormat("{0}, ", numberString.Substring(0, numberString.Length > 4 ? 4 : numberString.Length));
                }
                sb.AppendLine();
            }

            sb.AppendLine(ReferencePicURL);

            return sb.ToString();
        }
    }
}