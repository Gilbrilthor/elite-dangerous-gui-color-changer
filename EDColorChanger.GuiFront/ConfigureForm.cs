﻿using EDColorChanger.GuiFront.Properties;
using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EDColorChanger.GuiFront
{
    public partial class ConfigureForm : Form
    {
        // Tags used to identify a textbox for deciding whether to use a file browser or a folder browser
        private const string fileTag = "file";

        private const string folderTag = "folder";

        // Path chain from the AppData folder to the screenshot folder. {0} => My Pictures
        private const string screenshotFolderPathFormat = @"{0}\Frontier Developments\Elite Dangerous";

        public ConfigureForm()
        {
            InitializeComponent();

            // Load all the settings into the form
            LoadSettingsToForm();
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            // Get the control before this one
            var textBox = this.GetNextControl((Button)sender, false) as TextBox;

            if (textBox == null)
            {
                return;
            }

            var tag = textBox.Tag as string;

            if (tag == fileTag)
            {
                var dlg = new OpenFileDialog
                {
                    FileName = textBox.Text,
                    Multiselect = false
                };

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    textBox.Text = dlg.FileName;
                }
            }
            else if (tag == folderTag)
            {
                var dlg = new FolderBrowserDialog { SelectedPath = textBox.Text, Description = "Select a folder to use" };

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    textBox.Text = dlg.SelectedPath;
                }
            }
            else
            {
                return;
            }
        }

        private string FindGraphicsConfigurationPath()
        {
            // Kindly provided by Bilago on the Elite Dangerous SubReddit here:
            // http: //www.reddit.com/r/EliteDangerous/comments/2qy4xo/updated_elite_dangerous_color_changer_with_custom/cnb34di

            var reg32path = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{696F8871-C91D-4CB1-825D-36BE18065575}_is1";
            var reg64path = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{696F8871-C91D-4CB1-825D-36BE18065575}_is1";
            var default32Path = Path.Combine(Path.GetPathRoot(Environment.SystemDirectory), "program files\\Frontier\\EDLaunch\\Products\\FORC-FDEV-D-1003\\GraphicsConfiguration.xml");
            var default64Path = Path.Combine(Path.GetPathRoot(Environment.SystemDirectory), "program files(x86)\\Frontier\\EDLaunch\\Products\\FORC-FDEV-D-1003\\GraphicsConfiguration.xml");
            var rootDir = "";

            //Lets check the obvious static folders that it would default install, since this will be most common and save from accessing the registry

            if (File.Exists(default64Path))
                return default64Path;
            else if (File.Exists(default32Path))
                return default32Path;

            //Now since it isn't in the default directories, lets query the database
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(reg64path, false))
            {
                if (key != null)
                    if (key.GetValue("InstallLocation", null) != null)
                        rootDir = (string)key.GetValue("InstallLocation", null);
            }
            //If rootDir wasn't assigned a value, then we're on a 32bit OS (rare) lets check 32 bit path now
            if (string.IsNullOrEmpty(rootDir))
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(reg32path, false))
                {
                    if (key != null)
                        if (key.GetValue("InstallLocation", null) != null)
                            rootDir = (string)key.GetValue("InstallLocation", null);
                }

            // Look recursively for the file, because the FORC-FDEV-D-10* numbers can change

            return Directory.GetFiles(Path.Combine(rootDir, "Products\\"), @"GraphicsConfiguration.xml", SearchOption.AllDirectories).FirstOrDefault();
        }

        private string FindScreenshotsFolder()
        {
            var path = string.Format(screenshotFolderPathFormat,
                Environment.GetFolderPath(Environment.SpecialFolder.MyPictures));

            return Directory.Exists(path) ? path : null;
        }

        private void invalidateBackupButton_Click(object sender, EventArgs e)
        {
            Settings.Default.CompletedBackup = false;
            invalidateBackupButton.Enabled = false;
        }

        /// <summary>
        /// Loads the settings in to the form.
        /// </summary>
        private void LoadSettingsToForm()
        {
            // Pull the settings from the settings file
            graphicsFilenameBox.Text = Settings.Default.GraphicConfigFilename;
            backupFilenameBox.Text = Settings.Default.BackupFilename;
            schemeUrlBox.Text = Settings.Default.SchemeURL;
            applyTextButton.Text = Settings.Default.ApplyText;
            screenshotsFolderPathBox.Text = Settings.Default.ScreenshotFolderPath;
            autoRetrieveCheckBox.Checked = Settings.Default.ShouldRetrieveSchemesOnStartup;

            // If it's the first time, find the screenshots and the config file paths
            if (!Settings.Default.CompletedInitialSetup)
            {
                screenshotsFolderPathBox.Text = FindScreenshotsFolder();
                graphicsFilenameBox.Text = FindGraphicsConfigurationPath();
            }

            // If they've done a backup, allow them to force a backup again.
            if (Settings.Default.CompletedBackup)
            {
                invalidateBackupButton.Enabled = true;
            }
        }

        private void okayButton_Click(object sender, EventArgs e)
        {
            // Save the settings back into the file before closing
            SaveSettingsFromForm();
            this.Close();
        }

        /// <summary>
        /// Saves the settings from the form into Settings.Settings.
        /// </summary>
        private void SaveSettingsFromForm()
        {
            // Put the settings back into Settings.Settings
            Settings.Default.GraphicConfigFilename = graphicsFilenameBox.Text;
            Settings.Default.BackupFilename = backupFilenameBox.Text;
            Settings.Default.SchemeURL = schemeUrlBox.Text;
            Settings.Default.ApplyText = applyTextButton.Text;
            Settings.Default.ScreenshotFolderPath = screenshotsFolderPathBox.Text;
            Settings.Default.ShouldRetrieveSchemesOnStartup = autoRetrieveCheckBox.Checked;

            // This must be done before the save, otherwise, it will always do the initial setup
            Settings.Default.CompletedInitialSetup = true;
            Settings.Default.Save();
        }
    }
}