﻿namespace EDColorChanger.GuiFront
{
    partial class ErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.errorTextBox = new System.Windows.Forms.TextBox();
            this.goToRedditButton = new System.Windows.Forms.Button();
            this.goToBitbucketButton = new System.Windows.Forms.Button();
            this.doNothingButton = new System.Windows.Forms.Button();
            this.copytToClipboardButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(521, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Uh oh! It appears that something has gone horribly wrong! I\'m sorry about that. C" +
    "ould you send this log to me?";
            // 
            // errorTextBox
            // 
            this.errorTextBox.Location = new System.Drawing.Point(13, 30);
            this.errorTextBox.Multiline = true;
            this.errorTextBox.Name = "errorTextBox";
            this.errorTextBox.ReadOnly = true;
            this.errorTextBox.Size = new System.Drawing.Size(517, 244);
            this.errorTextBox.TabIndex = 1;
            // 
            // goToRedditButton
            // 
            this.goToRedditButton.Location = new System.Drawing.Point(13, 320);
            this.goToRedditButton.Name = "goToRedditButton";
            this.goToRedditButton.Size = new System.Drawing.Size(115, 23);
            this.goToRedditButton.TabIndex = 2;
            this.goToRedditButton.Text = "Find me on Reddit...";
            this.goToRedditButton.UseVisualStyleBackColor = true;
            this.goToRedditButton.Click += new System.EventHandler(this.goToRedditButton_Click);
            // 
            // goToBitbucketButton
            // 
            this.goToBitbucketButton.Location = new System.Drawing.Point(135, 320);
            this.goToBitbucketButton.Name = "goToBitbucketButton";
            this.goToBitbucketButton.Size = new System.Drawing.Size(153, 23);
            this.goToBitbucketButton.TabIndex = 3;
            this.goToBitbucketButton.Text = "Post an issue on BitBucket...";
            this.goToBitbucketButton.UseVisualStyleBackColor = true;
            this.goToBitbucketButton.Click += new System.EventHandler(this.goToBitbucketButton_Click);
            // 
            // doNothingButton
            // 
            this.doNothingButton.Location = new System.Drawing.Point(295, 320);
            this.doNothingButton.Name = "doNothingButton";
            this.doNothingButton.Size = new System.Drawing.Size(235, 23);
            this.doNothingButton.TabIndex = 4;
            this.doNothingButton.Text = "Ignore the issue and hope it goes away";
            this.doNothingButton.UseVisualStyleBackColor = true;
            this.doNothingButton.Click += new System.EventHandler(this.doNothingButton_Click);
            // 
            // copytToClipboardButton
            // 
            this.copytToClipboardButton.Location = new System.Drawing.Point(13, 281);
            this.copytToClipboardButton.Name = "copytToClipboardButton";
            this.copytToClipboardButton.Size = new System.Drawing.Size(517, 33);
            this.copytToClipboardButton.TabIndex = 5;
            this.copytToClipboardButton.Text = "Copy log to Clipboard";
            this.copytToClipboardButton.UseVisualStyleBackColor = true;
            this.copytToClipboardButton.Click += new System.EventHandler(this.copyToClipboardButton_Click);
            // 
            // ErrorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 355);
            this.Controls.Add(this.copytToClipboardButton);
            this.Controls.Add(this.doNothingButton);
            this.Controls.Add(this.goToBitbucketButton);
            this.Controls.Add(this.goToRedditButton);
            this.Controls.Add(this.errorTextBox);
            this.Controls.Add(this.label1);
            this.Name = "ErrorForm";
            this.Text = "Oops!";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ErrorForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox errorTextBox;
        private System.Windows.Forms.Button goToRedditButton;
        private System.Windows.Forms.Button goToBitbucketButton;
        private System.Windows.Forms.Button doNothingButton;
        private System.Windows.Forms.Button copytToClipboardButton;
    }
}