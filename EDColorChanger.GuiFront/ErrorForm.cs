﻿using EDColorChanger.GuiFront.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EDColorChanger.GuiFront
{
    public partial class ErrorForm : Form
    {
        // The exception passed to the form
        private readonly Exception _ex;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorForm"/> class.
        /// </summary>
        /// <param name="ex">The exception to use in populating the form.</param>
        public ErrorForm(Exception ex)
        {
            InitializeComponent();

            // Set the textbox text, then save the exception
            errorTextBox.Text = ex.ToString();
            _ex = ex;
        }

        private void copyToClipboardButton_Click(object sender, EventArgs e)
        {
            GuiHelper.SendTextToClipboard(_ex.ToString());
        }

        private void doNothingButton_Click(object sender, EventArgs e)
        {
            // Don't do anything, as the name implies
            this.Close();
        }

        private void ErrorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // See if the user would like to restart
            if (MessageBox.Show("Would you like to restart?", "Restart?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                Application.Restart();
        }

        private void goToBitbucketButton_Click(object sender, EventArgs e)
        {
            GuiHelper.OpenLinkInDefaultBrowser(Settings.Default.BitBucketIssuesLink);
        }

        private void goToRedditButton_Click(object sender, EventArgs e)
        {
            GuiHelper.OpenLinkInDefaultBrowser(Settings.Default.RedditLink);
        }
    }
}