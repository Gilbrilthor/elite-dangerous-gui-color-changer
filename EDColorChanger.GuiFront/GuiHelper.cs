﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace EDColorChanger.GuiFront
{
    internal class GuiHelper
    {
        /// <summary>
        /// Opens the link in the user's default browser.
        /// </summary>
        /// <param name="link">The link to open.</param>
        public static void OpenLinkInDefaultBrowser(string link)
        {
            // Passing a URL into the starting of a process triggers the default browser.
            Process.Start(link);
        }

        /// <summary>
        /// Sends the text to clipboard and displays a message indicating that it has been done.
        /// </summary>
        /// <param name="clipboardText">The clipboard text.</param>
        /// <param name="messageText">
        /// The message text to display on the box. If null, show "Text copied to Clipboard!".
        /// </param>
        public static void SendTextToClipboard(string clipboardText, string messageText = null)
        {
            // Set the text of the clipboard
            Clipboard.SetText(clipboardText);

            var text = messageText ?? "Text copied to Clipboard!";

            // Show the messagebox. If there's no format, then it should just continue
            MessageBox.Show(
                String.Format(text, clipboardText),
                "Sent to Clipboard");
        }
    }
}