﻿namespace EDColorChanger.GuiFront
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.previewBox = new System.Windows.Forms.PictureBox();
            this.retrieveSchemesButton = new System.Windows.Forms.Button();
            this.schemeListBox = new System.Windows.Forms.ListBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.authorLabel = new System.Windows.Forms.Label();
            this.replaceButton = new System.Windows.Forms.Button();
            this.configureButton = new System.Windows.Forms.Button();
            this.reportBugLabel = new System.Windows.Forms.LinkLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.openScreenshotFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.oneSchemeTab = new System.Windows.Forms.TabPage();
            this.beforeAfterTab = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.beforePicBox = new System.Windows.Forms.PictureBox();
            this.afterPicBox = new System.Windows.Forms.PictureBox();
            this.customTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.customPicBox = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel0 = new System.Windows.Forms.Panel();
            this.trackBar0 = new System.Windows.Forms.TrackBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.trackBar4 = new System.Windows.Forms.TrackBar();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.trackBar5 = new System.Windows.Forms.TrackBar();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.trackBar6 = new System.Windows.Forms.TrackBar();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.trackBar7 = new System.Windows.Forms.TrackBar();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.trackBar8 = new System.Windows.Forms.TrackBar();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.exportSchemeButton = new System.Windows.Forms.Button();
            this.resetColorsButton = new System.Windows.Forms.Button();
            this.clampCheckBox = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.b3Label = new System.Windows.Forms.Label();
            this.b2Label = new System.Windows.Forms.Label();
            this.b1Label = new System.Windows.Forms.Label();
            this.g3Label = new System.Windows.Forms.Label();
            this.g2Label = new System.Windows.Forms.Label();
            this.g1Label = new System.Windows.Forms.Label();
            this.r3Label = new System.Windows.Forms.Label();
            this.r2Label = new System.Windows.Forms.Label();
            this.r1Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.oneSchemeTab.SuspendLayout();
            this.beforeAfterTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.beforePicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.afterPicBox)).BeginInit();
            this.customTabPage.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customPicBox)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar0)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar6)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar7)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar8)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // previewBox
            // 
            this.previewBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.previewBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewBox.Location = new System.Drawing.Point(3, 3);
            this.previewBox.Name = "previewBox";
            this.previewBox.Size = new System.Drawing.Size(970, 434);
            this.previewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.previewBox.TabIndex = 0;
            this.previewBox.TabStop = false;
            // 
            // retrieveSchemesButton
            // 
            this.retrieveSchemesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.retrieveSchemesButton.Location = new System.Drawing.Point(11, 68);
            this.retrieveSchemesButton.Name = "retrieveSchemesButton";
            this.retrieveSchemesButton.Size = new System.Drawing.Size(123, 23);
            this.retrieveSchemesButton.TabIndex = 1;
            this.retrieveSchemesButton.Text = "Get Schemes Online...";
            this.retrieveSchemesButton.UseVisualStyleBackColor = true;
            this.retrieveSchemesButton.Click += new System.EventHandler(this.retrieveSchemesButton_Click);
            // 
            // schemeListBox
            // 
            this.schemeListBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.schemeListBox.DisplayMember = "Name";
            this.schemeListBox.FormattingEnabled = true;
            this.schemeListBox.Location = new System.Drawing.Point(140, 9);
            this.schemeListBox.Name = "schemeListBox";
            this.schemeListBox.Size = new System.Drawing.Size(238, 82);
            this.schemeListBox.TabIndex = 2;
            this.schemeListBox.SelectedIndexChanged += new System.EventHandler(this.schemeListBox_SelectedIndexChanged);
            // 
            // nameLabel
            // 
            this.nameLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(385, 10);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(163, 25);
            this.nameLabel.TabIndex = 3;
            this.nameLabel.Text = "Scheme Name";
            // 
            // authorLabel
            // 
            this.authorLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.authorLabel.AutoSize = true;
            this.authorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authorLabel.Location = new System.Drawing.Point(385, 39);
            this.authorLabel.Name = "authorLabel";
            this.authorLabel.Size = new System.Drawing.Size(46, 16);
            this.authorLabel.TabIndex = 4;
            this.authorLabel.Text = "Author";
            // 
            // replaceButton
            // 
            this.replaceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceButton.Enabled = false;
            this.replaceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.replaceButton.Location = new System.Drawing.Point(814, 10);
            this.replaceButton.Name = "replaceButton";
            this.replaceButton.Size = new System.Drawing.Size(161, 81);
            this.replaceButton.TabIndex = 5;
            this.replaceButton.Text = "Make It So";
            this.replaceButton.UseVisualStyleBackColor = true;
            this.replaceButton.Click += new System.EventHandler(this.replaceButton_Click);
            // 
            // configureButton
            // 
            this.configureButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.configureButton.Location = new System.Drawing.Point(11, 10);
            this.configureButton.Name = "configureButton";
            this.configureButton.Size = new System.Drawing.Size(123, 23);
            this.configureButton.TabIndex = 6;
            this.configureButton.Text = "Configure...";
            this.configureButton.UseVisualStyleBackColor = true;
            this.configureButton.Click += new System.EventHandler(this.configureButton_Click);
            // 
            // reportBugLabel
            // 
            this.reportBugLabel.AutoSize = true;
            this.reportBugLabel.Location = new System.Drawing.Point(828, 9);
            this.reportBugLabel.Name = "reportBugLabel";
            this.reportBugLabel.Size = new System.Drawing.Size(149, 13);
            this.reportBugLabel.TabIndex = 7;
            this.reportBugLabel.TabStop = true;
            this.reportBugLabel.Text = "Found a bug? Report it here...";
            this.reportBugLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.reportBugLabel_LinkClicked);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openScreenshotFolderToolStripMenuItem,
            this.checkForUpdatesToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // openScreenshotFolderToolStripMenuItem
            // 
            this.openScreenshotFolderToolStripMenuItem.Enabled = false;
            this.openScreenshotFolderToolStripMenuItem.Name = "openScreenshotFolderToolStripMenuItem";
            this.openScreenshotFolderToolStripMenuItem.Size = new System.Drawing.Size(154, 20);
            this.openScreenshotFolderToolStripMenuItem.Text = "Open Screenshot Folder...";
            this.openScreenshotFolderToolStripMenuItem.Click += new System.EventHandler(this.openScreenshotFolderToolStripMenuItem_Click);
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            this.checkForUpdatesToolStripMenuItem.Size = new System.Drawing.Size(125, 20);
            this.checkForUpdatesToolStripMenuItem.Text = "Check for Updates...";
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.checkForUpdatesToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.aboutToolStripMenuItem.Text = "About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.oneSchemeTab);
            this.tabControl1.Controls.Add(this.beforeAfterTab);
            this.tabControl1.Controls.Add(this.customTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(984, 466);
            this.tabControl1.TabIndex = 9;
            // 
            // oneSchemeTab
            // 
            this.oneSchemeTab.Controls.Add(this.previewBox);
            this.oneSchemeTab.Location = new System.Drawing.Point(4, 22);
            this.oneSchemeTab.Name = "oneSchemeTab";
            this.oneSchemeTab.Padding = new System.Windows.Forms.Padding(3);
            this.oneSchemeTab.Size = new System.Drawing.Size(976, 440);
            this.oneSchemeTab.TabIndex = 0;
            this.oneSchemeTab.Text = "1 Scheme";
            this.oneSchemeTab.UseVisualStyleBackColor = true;
            // 
            // beforeAfterTab
            // 
            this.beforeAfterTab.Controls.Add(this.splitContainer1);
            this.beforeAfterTab.Location = new System.Drawing.Point(4, 22);
            this.beforeAfterTab.Name = "beforeAfterTab";
            this.beforeAfterTab.Padding = new System.Windows.Forms.Padding(3);
            this.beforeAfterTab.Size = new System.Drawing.Size(976, 440);
            this.beforeAfterTab.TabIndex = 1;
            this.beforeAfterTab.Text = "Before / After";
            this.beforeAfterTab.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.beforePicBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.afterPicBox);
            this.splitContainer1.Size = new System.Drawing.Size(970, 434);
            this.splitContainer1.SplitterDistance = 485;
            this.splitContainer1.TabIndex = 1;
            // 
            // beforePicBox
            // 
            this.beforePicBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.beforePicBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.beforePicBox.Location = new System.Drawing.Point(0, 0);
            this.beforePicBox.Name = "beforePicBox";
            this.beforePicBox.Size = new System.Drawing.Size(485, 434);
            this.beforePicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.beforePicBox.TabIndex = 0;
            this.beforePicBox.TabStop = false;
            // 
            // afterPicBox
            // 
            this.afterPicBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.afterPicBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.afterPicBox.Location = new System.Drawing.Point(0, 0);
            this.afterPicBox.Name = "afterPicBox";
            this.afterPicBox.Size = new System.Drawing.Size(481, 434);
            this.afterPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.afterPicBox.TabIndex = 1;
            this.afterPicBox.TabStop = false;
            // 
            // customTabPage
            // 
            this.customTabPage.Controls.Add(this.tableLayoutPanel2);
            this.customTabPage.Location = new System.Drawing.Point(4, 22);
            this.customTabPage.Name = "customTabPage";
            this.customTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.customTabPage.Size = new System.Drawing.Size(976, 440);
            this.customTabPage.TabIndex = 2;
            this.customTabPage.Text = "Custom";
            this.customTabPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 370F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.customPicBox, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(970, 434);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // customPicBox
            // 
            this.customPicBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.customPicBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customPicBox.Location = new System.Drawing.Point(373, 3);
            this.customPicBox.Name = "customPicBox";
            this.customPicBox.Size = new System.Drawing.Size(594, 428);
            this.customPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.customPicBox.TabIndex = 0;
            this.customPicBox.TabStop = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox2);
            this.flowLayoutPanel1.Controls.Add(this.groupBox3);
            this.flowLayoutPanel1.Controls.Add(this.groupBox4);
            this.flowLayoutPanel1.Controls.Add(this.groupBox5);
            this.flowLayoutPanel1.Controls.Add(this.groupBox6);
            this.flowLayoutPanel1.Controls.Add(this.groupBox7);
            this.flowLayoutPanel1.Controls.Add(this.groupBox8);
            this.flowLayoutPanel1.Controls.Add(this.groupBox9);
            this.flowLayoutPanel1.Controls.Add(this.groupBox10);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(364, 428);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.panel0);
            this.groupBox1.Controls.Add(this.trackBar0);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(115, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Tag = "Red";
            this.groupBox1.Text = "Red => Red";
            // 
            // panel0
            // 
            this.panel0.BackColor = System.Drawing.Color.Red;
            this.panel0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel0.Location = new System.Drawing.Point(7, 72);
            this.panel0.Name = "panel0";
            this.panel0.Size = new System.Drawing.Size(102, 23);
            this.panel0.TabIndex = 1;
            // 
            // trackBar0
            // 
            this.trackBar0.LargeChange = 25;
            this.trackBar0.Location = new System.Drawing.Point(7, 20);
            this.trackBar0.Maximum = 200;
            this.trackBar0.Name = "trackBar0";
            this.trackBar0.Size = new System.Drawing.Size(102, 45);
            this.trackBar0.TabIndex = 0;
            this.trackBar0.TickFrequency = 25;
            this.trackBar0.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar0.Value = 200;
            this.trackBar0.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Controls.Add(this.trackBar1);
            this.groupBox2.Location = new System.Drawing.Point(124, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 101);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Tag = "Red";
            this.groupBox2.Text = "Red => Green";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Location = new System.Drawing.Point(7, 72);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(102, 23);
            this.panel3.TabIndex = 1;
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 25;
            this.trackBar1.Location = new System.Drawing.Point(7, 20);
            this.trackBar1.Maximum = 200;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(102, 45);
            this.trackBar1.SmallChange = 10;
            this.trackBar1.TabIndex = 0;
            this.trackBar1.TickFrequency = 25;
            this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar1.Value = 100;
            this.trackBar1.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.panel4);
            this.groupBox3.Controls.Add(this.trackBar2);
            this.groupBox3.Location = new System.Drawing.Point(245, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(115, 101);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Tag = "Red";
            this.groupBox3.Text = "Red => Blue";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Location = new System.Drawing.Point(7, 72);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(102, 23);
            this.panel4.TabIndex = 1;
            // 
            // trackBar2
            // 
            this.trackBar2.LargeChange = 25;
            this.trackBar2.Location = new System.Drawing.Point(7, 20);
            this.trackBar2.Maximum = 200;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(102, 45);
            this.trackBar2.SmallChange = 10;
            this.trackBar2.TabIndex = 0;
            this.trackBar2.TickFrequency = 25;
            this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar2.Value = 100;
            this.trackBar2.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.panel5);
            this.groupBox4.Controls.Add(this.trackBar3);
            this.groupBox4.Location = new System.Drawing.Point(3, 110);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(115, 101);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Tag = "Green";
            this.groupBox4.Text = "Green => Red";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Location = new System.Drawing.Point(7, 72);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(102, 23);
            this.panel5.TabIndex = 1;
            // 
            // trackBar3
            // 
            this.trackBar3.LargeChange = 25;
            this.trackBar3.Location = new System.Drawing.Point(7, 20);
            this.trackBar3.Maximum = 200;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(102, 45);
            this.trackBar3.SmallChange = 10;
            this.trackBar3.TabIndex = 0;
            this.trackBar3.TickFrequency = 25;
            this.trackBar3.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar3.Value = 100;
            this.trackBar3.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.panel6);
            this.groupBox5.Controls.Add(this.trackBar4);
            this.groupBox5.Location = new System.Drawing.Point(124, 110);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(115, 101);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Tag = "Green";
            this.groupBox5.Text = "Green => Green";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Green;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Location = new System.Drawing.Point(7, 72);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(102, 23);
            this.panel6.TabIndex = 1;
            // 
            // trackBar4
            // 
            this.trackBar4.LargeChange = 25;
            this.trackBar4.Location = new System.Drawing.Point(7, 20);
            this.trackBar4.Maximum = 200;
            this.trackBar4.Name = "trackBar4";
            this.trackBar4.Size = new System.Drawing.Size(102, 45);
            this.trackBar4.SmallChange = 10;
            this.trackBar4.TabIndex = 0;
            this.trackBar4.TickFrequency = 25;
            this.trackBar4.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar4.Value = 200;
            this.trackBar4.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.panel7);
            this.groupBox6.Controls.Add(this.trackBar5);
            this.groupBox6.Location = new System.Drawing.Point(245, 110);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(115, 101);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Tag = "Green";
            this.groupBox6.Text = "Green => Blue";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Black;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Location = new System.Drawing.Point(7, 72);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(102, 23);
            this.panel7.TabIndex = 1;
            // 
            // trackBar5
            // 
            this.trackBar5.LargeChange = 25;
            this.trackBar5.Location = new System.Drawing.Point(7, 20);
            this.trackBar5.Maximum = 200;
            this.trackBar5.Name = "trackBar5";
            this.trackBar5.Size = new System.Drawing.Size(102, 45);
            this.trackBar5.SmallChange = 10;
            this.trackBar5.TabIndex = 0;
            this.trackBar5.TickFrequency = 25;
            this.trackBar5.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar5.Value = 100;
            this.trackBar5.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.panel8);
            this.groupBox7.Controls.Add(this.trackBar6);
            this.groupBox7.Location = new System.Drawing.Point(3, 217);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(115, 101);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            this.groupBox7.Tag = "Blue";
            this.groupBox7.Text = "Blue => Red";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Black;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Location = new System.Drawing.Point(7, 72);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(102, 23);
            this.panel8.TabIndex = 1;
            // 
            // trackBar6
            // 
            this.trackBar6.LargeChange = 25;
            this.trackBar6.Location = new System.Drawing.Point(7, 20);
            this.trackBar6.Maximum = 200;
            this.trackBar6.Name = "trackBar6";
            this.trackBar6.Size = new System.Drawing.Size(102, 45);
            this.trackBar6.SmallChange = 10;
            this.trackBar6.TabIndex = 0;
            this.trackBar6.TickFrequency = 25;
            this.trackBar6.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar6.Value = 100;
            this.trackBar6.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.panel9);
            this.groupBox8.Controls.Add(this.trackBar7);
            this.groupBox8.Location = new System.Drawing.Point(124, 217);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(115, 101);
            this.groupBox8.TabIndex = 7;
            this.groupBox8.TabStop = false;
            this.groupBox8.Tag = "Blue";
            this.groupBox8.Text = "Blue => Green";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Black;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Location = new System.Drawing.Point(7, 72);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(102, 23);
            this.panel9.TabIndex = 1;
            // 
            // trackBar7
            // 
            this.trackBar7.LargeChange = 25;
            this.trackBar7.Location = new System.Drawing.Point(7, 20);
            this.trackBar7.Maximum = 200;
            this.trackBar7.Name = "trackBar7";
            this.trackBar7.Size = new System.Drawing.Size(102, 45);
            this.trackBar7.SmallChange = 10;
            this.trackBar7.TabIndex = 0;
            this.trackBar7.TickFrequency = 25;
            this.trackBar7.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar7.Value = 100;
            this.trackBar7.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.panel10);
            this.groupBox9.Controls.Add(this.trackBar8);
            this.groupBox9.Location = new System.Drawing.Point(245, 217);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(115, 101);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            this.groupBox9.Tag = "Blue";
            this.groupBox9.Text = "Blue => Blue";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Blue;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Location = new System.Drawing.Point(7, 72);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(102, 23);
            this.panel10.TabIndex = 1;
            // 
            // trackBar8
            // 
            this.trackBar8.LargeChange = 25;
            this.trackBar8.Location = new System.Drawing.Point(7, 20);
            this.trackBar8.Maximum = 200;
            this.trackBar8.Name = "trackBar8";
            this.trackBar8.Size = new System.Drawing.Size(102, 45);
            this.trackBar8.SmallChange = 10;
            this.trackBar8.TabIndex = 0;
            this.trackBar8.TickFrequency = 25;
            this.trackBar8.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBar8.Value = 200;
            this.trackBar8.Scroll += new System.EventHandler(this.colorMatrixTrackbar_Scroll);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.exportSchemeButton);
            this.groupBox10.Controls.Add(this.resetColorsButton);
            this.groupBox10.Controls.Add(this.clampCheckBox);
            this.groupBox10.Location = new System.Drawing.Point(3, 324);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(357, 100);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Options";
            // 
            // exportSchemeButton
            // 
            this.exportSchemeButton.Location = new System.Drawing.Point(7, 71);
            this.exportSchemeButton.Name = "exportSchemeButton";
            this.exportSchemeButton.Size = new System.Drawing.Size(139, 23);
            this.exportSchemeButton.TabIndex = 2;
            this.exportSchemeButton.Text = "Export as Scheme Line...";
            this.exportSchemeButton.UseVisualStyleBackColor = true;
            this.exportSchemeButton.Click += new System.EventHandler(this.exportSchemeButton_Click);
            // 
            // resetColorsButton
            // 
            this.resetColorsButton.Location = new System.Drawing.Point(249, 71);
            this.resetColorsButton.Name = "resetColorsButton";
            this.resetColorsButton.Size = new System.Drawing.Size(102, 23);
            this.resetColorsButton.TabIndex = 1;
            this.resetColorsButton.Text = "Reset Values...";
            this.resetColorsButton.UseVisualStyleBackColor = true;
            this.resetColorsButton.Click += new System.EventHandler(this.resetColorsButton_Click);
            // 
            // clampCheckBox
            // 
            this.clampCheckBox.AutoSize = true;
            this.clampCheckBox.Location = new System.Drawing.Point(7, 20);
            this.clampCheckBox.Name = "clampCheckBox";
            this.clampCheckBox.Size = new System.Drawing.Size(139, 17);
            this.clampCheckBox.TabIndex = 0;
            this.clampCheckBox.Text = "Clamp Row Totals to 1?";
            this.clampCheckBox.UseVisualStyleBackColor = true;
            this.clampCheckBox.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.nameLabel);
            this.panel1.Controls.Add(this.schemeListBox);
            this.panel1.Controls.Add(this.authorLabel);
            this.panel1.Controls.Add(this.configureButton);
            this.panel1.Controls.Add(this.replaceButton);
            this.panel1.Controls.Add(this.retrieveSchemesButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 490);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 103);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Location = new System.Drawing.Point(685, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(123, 81);
            this.panel2.TabIndex = 8;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.b3Label, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.b2Label, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.b1Label, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.g3Label, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.g2Label, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.g1Label, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.r3Label, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.r2Label, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.r1Label, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, -2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(115, 82);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // b3Label
            // 
            this.b3Label.AutoSize = true;
            this.b3Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b3Label.Location = new System.Drawing.Point(79, 54);
            this.b3Label.Name = "b3Label";
            this.b3Label.Size = new System.Drawing.Size(33, 28);
            this.b3Label.TabIndex = 9;
            this.b3Label.Text = "b3";
            this.b3Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // b2Label
            // 
            this.b2Label.AutoSize = true;
            this.b2Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b2Label.Location = new System.Drawing.Point(41, 54);
            this.b2Label.Name = "b2Label";
            this.b2Label.Size = new System.Drawing.Size(32, 28);
            this.b2Label.TabIndex = 9;
            this.b2Label.Text = "b2";
            this.b2Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // b1Label
            // 
            this.b1Label.AutoSize = true;
            this.b1Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.b1Label.Location = new System.Drawing.Point(3, 54);
            this.b1Label.Name = "b1Label";
            this.b1Label.Size = new System.Drawing.Size(32, 28);
            this.b1Label.TabIndex = 9;
            this.b1Label.Text = "b1";
            this.b1Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g3Label
            // 
            this.g3Label.AutoSize = true;
            this.g3Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.g3Label.Location = new System.Drawing.Point(79, 27);
            this.g3Label.Name = "g3Label";
            this.g3Label.Size = new System.Drawing.Size(33, 27);
            this.g3Label.TabIndex = 9;
            this.g3Label.Text = "g3";
            this.g3Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g2Label
            // 
            this.g2Label.AutoSize = true;
            this.g2Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.g2Label.Location = new System.Drawing.Point(41, 27);
            this.g2Label.Name = "g2Label";
            this.g2Label.Size = new System.Drawing.Size(32, 27);
            this.g2Label.TabIndex = 9;
            this.g2Label.Text = "g2";
            this.g2Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // g1Label
            // 
            this.g1Label.AutoSize = true;
            this.g1Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.g1Label.Location = new System.Drawing.Point(3, 27);
            this.g1Label.Name = "g1Label";
            this.g1Label.Size = new System.Drawing.Size(32, 27);
            this.g1Label.TabIndex = 9;
            this.g1Label.Text = "g1";
            this.g1Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // r3Label
            // 
            this.r3Label.AutoSize = true;
            this.r3Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.r3Label.Location = new System.Drawing.Point(79, 0);
            this.r3Label.Name = "r3Label";
            this.r3Label.Size = new System.Drawing.Size(33, 27);
            this.r3Label.TabIndex = 9;
            this.r3Label.Text = "r3";
            this.r3Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // r2Label
            // 
            this.r2Label.AutoSize = true;
            this.r2Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.r2Label.Location = new System.Drawing.Point(41, 0);
            this.r2Label.Name = "r2Label";
            this.r2Label.Size = new System.Drawing.Size(32, 27);
            this.r2Label.TabIndex = 8;
            this.r2Label.Text = "r2";
            this.r2Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // r1Label
            // 
            this.r1Label.AutoSize = true;
            this.r1Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.r1Label.Location = new System.Drawing.Point(3, 0);
            this.r1Label.Name = "r1Label";
            this.r1Label.Size = new System.Drawing.Size(32, 27);
            this.r1Label.TabIndex = 0;
            this.r1Label.Text = "r1";
            this.r1Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 593);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.reportBugLabel);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1000, 631);
            this.Name = "MainForm";
            this.Text = "Elite Dangerous Color Changer";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.oneSchemeTab.ResumeLayout(false);
            this.beforeAfterTab.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.beforePicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.afterPicBox)).EndInit();
            this.customTabPage.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.customPicBox)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar0)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar6)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar7)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar8)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox previewBox;
        private System.Windows.Forms.Button retrieveSchemesButton;
        private System.Windows.Forms.ListBox schemeListBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label authorLabel;
        private System.Windows.Forms.Button replaceButton;
        private System.Windows.Forms.Button configureButton;
        private System.Windows.Forms.LinkLabel reportBugLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openScreenshotFolderToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage oneSchemeTab;
        private System.Windows.Forms.TabPage beforeAfterTab;
        private System.Windows.Forms.PictureBox afterPicBox;
        private System.Windows.Forms.PictureBox beforePicBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label b3Label;
        private System.Windows.Forms.Label b2Label;
        private System.Windows.Forms.Label b1Label;
        private System.Windows.Forms.Label g3Label;
        private System.Windows.Forms.Label g2Label;
        private System.Windows.Forms.Label g1Label;
        private System.Windows.Forms.Label r3Label;
        private System.Windows.Forms.Label r2Label;
        private System.Windows.Forms.Label r1Label;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabPage customTabPage;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel0;
        private System.Windows.Forms.TrackBar trackBar0;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TrackBar trackBar4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TrackBar trackBar5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TrackBar trackBar6;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TrackBar trackBar7;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TrackBar trackBar8;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button resetColorsButton;
        private System.Windows.Forms.CheckBox clampCheckBox;
        private System.Windows.Forms.PictureBox customPicBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button exportSchemeButton;
    }
}

