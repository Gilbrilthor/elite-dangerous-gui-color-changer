﻿using EDColorChanger.Core;
using EDColorChanger.GuiFront.Properties;
using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

namespace EDColorChanger.GuiFront
{
    public partial class MainForm : Form
    {
        // Stopgap to allow the custom picture to be saved to the pic dictionary
        private const string CUSTOM_PIC_KEY = "CUSTOM11337";

        // The cache dictionary to hold the images pulled from the net
        private readonly Dictionary<string, Image> _picDictionary;

        // The custom image after the EdColorMatrix has been applied to it
        private Image _customAfterImage;

        // The custom matrix for the custom scheme tab
        private EdColorMatrix _customMatrix;

        public MainForm()
        {
            Cursor.Current = Cursors.WaitCursor;
            InitializeComponent();
            _picDictionary = new Dictionary<string, Image>();
        }

        /// <summary>
        /// Reads the URL and return a picture.
        /// </summary>
        /// <param name="url">The URL to read.</param>
        /// <returns>an image, if the URL led to one. Otherwise, null.</returns>
        private static Image ReadPicUrl(string url)
        {
            // open a client and read the data
            var client = new WebClient();
            var stream = client.OpenRead(url);
            var reader = new StreamReader(stream);
            Image image = null;
            try
            {
                // Try to make an image from it.
                image = Image.FromStream(reader.BaseStream);
            }
            catch (Exception e)
            {
                // Do nothing right now.
                // TODO: Add logging
            }
            return image;
        }

        /// <summary>
        /// Reads the URL and returns the data as a string.
        /// </summary>
        /// <param name="url">The URL to read.</param>
        /// <returns>A string containing the page</returns>
        private static string ReadUrl(string url)
        {
            var client = new WebClient();
            var stream = client.OpenRead(url);
            var reader = new StreamReader(stream);
            var content = reader.ReadToEnd();

            return content;
        }

        /// <summary>
        /// Shows the configure form.
        /// </summary>
        private static void ShowConfigureForm()
        {
            var dlg = new ConfigureForm();

            dlg.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new AboutForm();

            form.Show();
        }

        /// <summary>
        /// Changes the selected scheme.
        /// </summary>
        /// <param name="matrix">The matrix to change the scheme to.</param>
        private void ChangeSelectedScheme(EdColorMatrix matrix)
        {
            // Set the labels on the bottom
            nameLabel.Text = matrix.Name;
            authorLabel.Text = string.Format("By {0}", matrix.AuthorName);

            Image img = null;

            // Get the image and apply it to both the preview on the first tab and the before/after tab
            img = GetImageForMatrix(matrix);

            previewBox.Image = img;
            afterPicBox.Image = img;
        }

        private void checkForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // This will fail if the application hasn't been deployed
            var appDep = ApplicationDeployment.CurrentDeployment;

            // Set a wait cursor to plead for the user's patience
            Cursor.Current = Cursors.WaitCursor;

            if (appDep.CheckForUpdate())
            {
                Cursor.Current = Cursors.Default;
                // Ask if you would like to update
                if (MessageBox.Show("An update was found. Would you like to update?", "Update Found",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Cursor.Current = Cursors.WaitCursor;

                    // Update
                    appDep.Update();

                    Cursor.Current = Cursors.Default;

                    // Tell update complete
                    MessageBox.Show("Update complete! The program will restart for new goodies!", "Update complete");
                    // Restart
                    Application.Restart();
                }
            }
            else
            {
                Cursor.Current = Cursors.Default;
                // Tell no update found
                MessageBox.Show(
                    "No update was found. Maybe you could suggest an enhancement if you are waiting for something.");
            }
        }

        private void colorMatrixTrackbar_Scroll(object sender, EventArgs e)
        {
            // Get the parent group box of the trackbar doing the scrolling
            var me = sender as TrackBar;
            var colorBox = me.Parent as GroupBox;

            // Find the default pic
            var img = _picDictionary["Default"];

            UpdateCustomColorGroupBox(colorBox);
            _customMatrix = UpdateCustomFormMatrix(img);
        }

        private void configureButton_Click(object sender, EventArgs e)
        {
            ShowConfigureForm();
            GetSettings();
        }

        private void exportSchemeButton_Click(object sender, EventArgs e)
        {
            var dlg = new SchemeExportForm(_customMatrix);

            dlg.ShowDialog();
        }

        /// <summary>
        /// Gets the image for matrix. Checks _picDictionary first. If it's not there, then it goes
        /// to the web using the matrix's referencePicUrl
        /// </summary>
        /// <param name="matrix">The matrix to get the image for.</param>
        /// <returns>An image related to the matrix. If there weren't any, return null.</returns>
        private Image GetImageForMatrix(EdColorMatrix matrix)
        {
            // Might could use
            // http: //spiffygif.com/?color=b41e73&corners=0&lines=5&bgColor=d0c1a2&trail=18&length=13&radius=17&width=13&halo=false
            // To show waiting
            Image img = null;

            // Check the cache first. Could be a quick win
            if (_picDictionary.ContainsKey(matrix.Name))
            {
                img = _picDictionary[matrix.Name];
            }
            else
            {
                // We have to go find an image now
                if (!string.IsNullOrWhiteSpace(matrix.ReferencePicURL))
                {
                    img = ReadPicUrl(matrix.ReferencePicURL);
                    previewBox.Image = img;
                    _picDictionary.Add(matrix.Name, img);
                }
                else
                {
                    img = null;
                }
            }
            return img;
        }

        /// <summary>
        /// Gets the matrices from the scheme list box.
        /// </summary>
        /// <returns>a list of the schemes contained in the scheme list box.</returns>
        private List<EdColorMatrix> GetMatricesFromListBox()
        {
            // You have to cast them here otherwise they are just objects
            var list = schemeListBox.Items.Cast<EdColorMatrix>().ToList();
            return list;
        }

        /// <summary>
        /// Gets the multiplier value for the RGB based on a scale from 0 to 200 =&gt; -1.0 to 1.0
        /// </summary>
        /// <param name="input">The input value from the trackbar.</param>
        /// <param name="lowClamp">The low clamp, if you want.</param>
        /// <param name="highClamp">The high clamp, if you want.</param>
        /// <returns></returns>
        private decimal GetMultiplierValue(int input, decimal? lowClamp = null, decimal? highClamp = null)
        {
            var result = (decimal)(input - 100) / 100;

            if (lowClamp.HasValue)
            {
                result = Math.Max(lowClamp.Value, result);
            }

            if (highClamp.HasValue)
            {
                result = Math.Min(highClamp.Value, result);
            }

            return result;
        }

        /// <summary>
        /// Gets the currently selected matrix.
        /// </summary>
        /// <returns>The matrix currently selected in the scheme list box</returns>
        private EdColorMatrix GetSelectedMatrix()
        {
            // Make sure there's something selected
            if (schemeListBox.SelectedIndex >= 0)
            {
                var m = schemeListBox.SelectedItem as EdColorMatrix;

                return m;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the settings and applies them to the form.
        /// </summary>
        private void GetSettings()
        {
            // Set the replace text to whatever they wanted
            replaceButton.Text = Settings.Default.ApplyText;

            // If we have a screenshot folder saved, enable the travel button
            if (!string.IsNullOrWhiteSpace(Settings.Default.ScreenshotFolderPath) && Directory.Exists(Settings.Default.ScreenshotFolderPath))
            {
                openScreenshotFolderToolStripMenuItem.Enabled = true;
            }
            else
            {
                openScreenshotFolderToolStripMenuItem.Enabled = false;
            }

            // If we have a graphics config saved, enable the replace button
            if (!string.IsNullOrWhiteSpace(Settings.Default.GraphicConfigFilename) && File.Exists(Settings.Default.GraphicConfigFilename))
            {
                replaceButton.Enabled = true;
            }
            else
            {
                replaceButton.Enabled = false;
            }
        }

        private void openScreenshotFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Opening the folder in this way will open an explorer
            try
            {
                Process.Start(Settings.Default.ScreenshotFolderPath);
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(string.Format("The folder '{0}' could not be found!", ex.FileName), "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Reads the schemes from the URL contained in settings.settings
        /// </summary>
        private void ReadSchemes()
        {
            // Clear the items out. Otherwise, we would have multiples
            schemeListBox.Items.Clear();

            // Get the schemes
            var result = ReadUrl(Settings.Default.SchemeURL);

            // Parse out each scheme, ignoring comment lines
            var lines =
                result.Split(new[] { "\n", "\n\r" }, StringSplitOptions.RemoveEmptyEntries).Where(l => !l.Trim().StartsWith("#"));

            // used to store matrices with bad data
            var badMatrixNames = new List<string>();
            // stores all good matrices
            var matrices = new List<EdColorMatrix>();
            foreach (var line in lines)
            {
                try
                {
                    var m = EdColorMatrix.BuildFromSchemeLine(line);
                    matrices.Add(m);
                }
                catch (FormatException e)
                {
                    badMatrixNames.Add(e.Data["MatrixName"] as string);
                }
            }

            // if there were any bad matrices, inform the user
            if (badMatrixNames.Any())
            {
                var sb = new StringBuilder();
                badMatrixNames.ForEach(name => sb.AppendFormat("{0}{1}", name, Environment.NewLine));

                MessageBox.Show(string.Format("Some schemes had bad matrices: {0}",
                    sb.ToString()));
            }

            // Add all the matrices, ordered by name and cast to objects to fit AddRanges requirements
            schemeListBox.Items.AddRange((from m in matrices.OrderBy(ma => ma.Name) select (object)m).ToArray());

            // Get the default image preloaded for the custom tab
            var defaultImg = GetImageForMatrix(matrices.First(m => m.Name == "Default"));
            _picDictionary["Default"] = defaultImg;
            customPicBox.Image = defaultImg;

            // Finally, set the before pic on the before/after tab
            SetBeforeImage();
        }

        private void replaceButton_Click(object sender, EventArgs e)
        {
            var grph = Settings.Default.GraphicConfigFilename;
            var back = Settings.Default.BackupFilename;

            // Make sure to get the selected matrix
            if (schemeListBox.SelectedIndex >= 0 && !string.IsNullOrWhiteSpace(grph))
            {
                // TODO: Add a check for no backup
                var m = schemeListBox.SelectedItem as EdColorMatrix;

                // change the color matrix. Pass the completed backup reversed so that it will
                // create a backup if there hasn't been one
                ColorChanger.ChangeColor(Settings.Default.GraphicConfigFilename,
                    m,
                    Settings.Default.BackupFilename,
                    !Settings.Default.CompletedBackup);

                // show a message box and then set the before image in the before/after tab
                MessageBox.Show("It is done, Commander");
                SetBeforeImage(m);
            }
            // check if its a custom matrix
            else if (tabControl1.SelectedTab == customTabPage && _customMatrix != null)
            {
                // Set the matrix to the custom matrix
                ColorChanger.ChangeColor(Settings.Default.GraphicConfigFilename,
                    _customMatrix,
                    Settings.Default.BackupFilename,
                    !Settings.Default.CompletedBackup);

                // show a message box and then set the before image in the before/after tab
                MessageBox.Show("It is done, Commander");
                SetBeforeImage(_customMatrix);
            }
        }

        private void reportBugLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GuiHelper.OpenLinkInDefaultBrowser(Settings.Default.BitBucketIssuesLink);
        }

        private void resetColorsButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Reset the values? This cannot be undone.", "Are you sure?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // Set all the values for the trackbars to their defaul values
                trackBar0.Value = 200;
                trackBar1.Value = 100;
                trackBar2.Value = 100;
                trackBar3.Value = 100;
                trackBar4.Value = 200;
                trackBar5.Value = 100;
                trackBar6.Value = 100;
                trackBar7.Value = 100;
                trackBar8.Value = 200;

                // Get the default image to use for the custom tab
                var img = _picDictionary["Default"];
                // update the color group boxes
                UpdateAllCustomColorGroupBoxes();
                // Set the _custom matrix back to the default and set the custom tab pic to the
                // default pic as well
                _customMatrix = UpdateCustomFormMatrix(img);
            }
        }

        private void retrieveSchemesButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Retrieve the schemes?", "Connect?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Cursor.Current = Cursors.WaitCursor;
                ReadSchemes();
                Cursor.Current = Cursors.Default;
            }
        }

        private void schemeListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var m = GetSelectedMatrix();

            if (m != null)
            {
                ChangeSelectedScheme(m);
                SetFormMatrix(m);
            }
        }

        private void SetBeforeImage(EdColorMatrix matrix = null)
        {
            Image img = null;

            // If a matrix was input, read it's image and set the stuff
            if (matrix != null)
            {
                img = GetImageForMatrix(matrix);
                ChangeSelectedScheme(matrix);
            }
            else
            {
                // Otherwise, try to read the scheme from the graphics config
                var configFilePath = Settings.Default.GraphicConfigFilename;
                if (string.IsNullOrWhiteSpace(configFilePath))
                {
                    return;
                }

                // Not sure if this is right or not. i thought the config file path was the file
                // itself If this is a directory instead of a file path, look for the file path
                if (Directory.Exists(configFilePath))
                {
                    var files = Directory.EnumerateFiles(configFilePath);
                    var configFileName = files.FirstOrDefault(s => s.ToLower().Contains("graphicsconfiguration.xml"));

                    if (!string.IsNullOrWhiteSpace(configFileName))
                    {
                        // If we found the file, then update the settings
                        configFilePath = configFileName;
                        Settings.Default.GraphicConfigFilename = configFilePath;
                        Settings.Default.Save();
                    }
                    else
                    {
                        // We couldn't find the file, say sorry and send them to look again
                        MessageBox.Show(
                            string.Format(
                                "'{0}' doesn't look like the configuration file. Try selecting the file directly in the configuration.",
                                configFilePath));
                        ShowConfigureForm();
                        return;
                    }
                }

                // Read the current matrix
                var readMatrix = ColorChanger.ReadColorMatrix(configFilePath);

                // Try to find matrix in the schemes
                var foundMatrix = GetMatricesFromListBox().FirstOrDefault(m => m.Equals(readMatrix));

                if (foundMatrix != null)
                {
                    // Set the image accordingly
                    img = GetImageForMatrix(foundMatrix);
                    ChangeSelectedScheme(foundMatrix);
                }
            }

            beforePicBox.Image = img;
        }

        /// <summary>
        /// Sets the table at the bottom based on a matrix.
        /// </summary>
        /// <param name="matrix">The matrix to pull the values from.</param>
        private void SetFormMatrix(EdColorMatrix matrix)
        {
            var matrixLabels = new[]
            {
                r1Label,
                r2Label,
                r3Label,
                g1Label,
                g2Label,
                g3Label,
                b1Label,
                b2Label,
                b3Label
            };

            var labelIndex = 0;

            for (int y = 0; y < matrix.Matrix.GetLength(0); y++)
            {
                for (int x = 0; x < matrix.Matrix.GetLength(1); x++)
                {
                    if (labelIndex < matrixLabels.Length)
                    {
                        matrixLabels[labelIndex++].Text = matrix.Matrix[x, y].ToString("F2");
                    }
                }
            }
        }

        /// <summary>
        /// Sets the form matrix based on loose values.
        /// </summary>
        /// <param name="r1">The r1.</param>
        /// <param name="r2">The r2.</param>
        /// <param name="r3">The r3.</param>
        /// <param name="g1">The g1.</param>
        /// <param name="g2">The g2.</param>
        /// <param name="g3">The g3.</param>
        /// <param name="b1">The b1.</param>
        /// <param name="b2">The b2.</param>
        /// <param name="b3">The b3.</param>
        /// <returns></returns>
        private EdColorMatrix SetFormMatrix(decimal r1, decimal r2, decimal r3,
            decimal g1, decimal g2, decimal g3,
            decimal b1, decimal b2, decimal b3)
        {
            // Create a new matrix and then call the overloaded method, passing that in
            var m = new EdColorMatrix();

            m.Matrix = new decimal[,]
            {
                {r1, g1, b1},
                {r2, g2, b2},
                {r3, g3, b3}
            };

            SetFormMatrix(m);

            // Return the matrix. Don't know if this is supposed to be here
            return m;
        }

        /// <summary>
        /// Updates all custom color group boxes. Basically takes their components and multiplies
        /// them by the values found from the trackbar
        /// </summary>
        private void UpdateAllCustomColorGroupBoxes()
        {
            // Get all the group boxes, copying them to an array so we can use linq
            var controls = new Control[customTabPage.Controls.Count];
            customTabPage.Controls.CopyTo(controls, 0);

            var groupBoxes = from c in controls where c is GroupBox select c as GroupBox;

            foreach (var groupBox in groupBoxes)
            {
                UpdateCustomColorGroupBox(groupBox);
            }
        }

        /// <summary>
        /// Updates the custom color group box, updating the color based on the associated trackbar
        /// </summary>
        /// <param name="colorBox">The color box to update.</param>
        private void UpdateCustomColorGroupBox(GroupBox colorBox)
        {
            // Find the color panel
            Panel panel = null;
            TrackBar bar = null;
            foreach (Control control in colorBox.Controls)
            {
                if (control.Name.Contains("panel"))
                {
                    panel = control as Panel;
                }
                if (control.Name.Contains("trackBar"))
                {
                    bar = control as TrackBar;
                }
            }

            var baseColor = Color.FromName((string)colorBox.Tag);

            // Clamp them to 0 through 1, otherwise it causes a multiplication issue
            var multiplier = GetMultiplierValue(bar.Value, 0, 1);

            var panelRed = baseColor.R * multiplier;
            var panelGreen = baseColor.G * multiplier;
            var panelBlue = baseColor.B * multiplier;

            // Update the color panel
            var panelColor = Color.FromArgb((int)panelRed, (int)panelGreen, (int)panelBlue);
            panel.BackColor = panelColor;
        }

        /// <summary>
        /// Updates the custom form matrix. It sets the after pic and returns the associated matrix
        /// </summary>
        /// <param name="beforePic">The before pic to shift according to the color matrix.</param>
        /// <returns>the matrix associated with the custom tab.</returns>
        private EdColorMatrix UpdateCustomFormMatrix(Image beforePic)
        {
            decimal bar0MultiplierValue = GetMultiplierValue(trackBar0.Value);
            decimal bar1MultiplierValue = GetMultiplierValue(trackBar1.Value);
            decimal bar2MultiplierValue = GetMultiplierValue(trackBar2.Value);
            decimal bar3MultiplierValue = GetMultiplierValue(trackBar3.Value);
            decimal bar4MultiplierValue = GetMultiplierValue(trackBar4.Value);
            decimal bar5MultiplierValue = GetMultiplierValue(trackBar5.Value);
            decimal bar6MultiplierValue = GetMultiplierValue(trackBar6.Value);
            decimal bar7MultiplierValue = GetMultiplierValue(trackBar7.Value);
            decimal bar8MultiplierValue = GetMultiplierValue(trackBar8.Value);

            var m = SetFormMatrix(
                bar0MultiplierValue,
                bar1MultiplierValue,
                bar2MultiplierValue,
                bar3MultiplierValue,
                bar4MultiplierValue,
                bar5MultiplierValue,
                bar6MultiplierValue,
                bar7MultiplierValue,
                bar8MultiplierValue
                );

            var colorMatrix = m.GetColorMatrix();

            // Set the attributes color matrix to the one output by the EdColorMatrix
            var attributes = new ImageAttributes();

            attributes.SetColorMatrix(colorMatrix);
            using (var img = new Bitmap(_picDictionary["Default"]))
            {
                // Create a graphics object, so we can use color matrix
                var g = Graphics.FromImage(img);

                // Copy it, paying attention to the color matrix using the attributes
                g.DrawImage(_picDictionary["Default"], new Rectangle(0, 0, img.Width, img.Height),
                    0, 0,
                    img.Width,
                    img.Height,
                    GraphicsUnit.Pixel,
                    attributes);

                // Set the after image. New object so that the using doesn't wipe it out
                _customAfterImage = new Bitmap(img);
            }

            // Set the custom picture to the one generated
            _picDictionary[CUSTOM_PIC_KEY] = _customAfterImage;
            customPicBox.Image = _customAfterImage;
            m.Name = CUSTOM_PIC_KEY;

            // TODO: Refactor out the double duty of creating and returning the matrix
            return m;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // If this is the user's first time, send them to go set up stuff before loading stuff
            if (!Settings.Default.CompletedInitialSetup)
            {
                ShowConfigureForm();
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
            }

            // If they want the schemes to pre-load, do that now
            if (Settings.Default.ShouldRetrieveSchemesOnStartup)
            {
                ReadSchemes();
            }

            GetSettings();

            Cursor.Current = Cursors.Default;
            this.Activate();
        }
    }
}