﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace EDColorChanger.GuiFront
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Put a stopgap that catches an error and provides some guidance about what to do
            try
            {
                var splashThread = SplashScreen.ShowSplashScreen();
                _mainForm = new MainForm();
                splashThread.Join();
                RunMainApplication();
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    throw;
                }
                new ErrorForm(ex).ShowDialog();
            }
        }

        private static Form _mainForm = null;

        public static void RunMainApplication()
        {
            Application.Run(_mainForm);
        }
    }
}