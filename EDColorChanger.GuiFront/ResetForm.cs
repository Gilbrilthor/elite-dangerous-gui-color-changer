﻿using EDColorChanger.GuiFront.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EDColorChanger.GuiFront
{
    public partial class ResetForm : Form
    {
        public ResetForm()
        {
            InitializeComponent();
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            ResetAllSettings();
        }

        private void ResetAllSettings()
        {
            if (MessageBox.Show("Are you sure you want to return to default settings?", "There is no turning back", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Settings.Default.Reset();
                Close();
            }
        }
    }
}