﻿namespace EDColorChanger.GuiFront
{
    partial class SchemeExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.schemeNameTextBox = new System.Windows.Forms.TextBox();
            this.authorNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.riftCompatibleCheckBox = new System.Windows.Forms.CheckBox();
            this.acceptButton = new System.Windows.Forms.Button();
            this.referencePicUrlTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Scheme Name";
            // 
            // schemeNameTextBox
            // 
            this.schemeNameTextBox.Location = new System.Drawing.Point(13, 30);
            this.schemeNameTextBox.Name = "schemeNameTextBox";
            this.schemeNameTextBox.Size = new System.Drawing.Size(230, 20);
            this.schemeNameTextBox.TabIndex = 0;
            // 
            // authorNameTextBox
            // 
            this.authorNameTextBox.Location = new System.Drawing.Point(13, 70);
            this.authorNameTextBox.Name = "authorNameTextBox";
            this.authorNameTextBox.Size = new System.Drawing.Size(230, 20);
            this.authorNameTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Author Name";
            // 
            // riftCompatibleCheckBox
            // 
            this.riftCompatibleCheckBox.AutoSize = true;
            this.riftCompatibleCheckBox.Location = new System.Drawing.Point(12, 139);
            this.riftCompatibleCheckBox.Name = "riftCompatibleCheckBox";
            this.riftCompatibleCheckBox.Size = new System.Drawing.Size(112, 17);
            this.riftCompatibleCheckBox.TabIndex = 3;
            this.riftCompatibleCheckBox.Text = "Is It Good for Rift?";
            this.riftCompatibleCheckBox.UseVisualStyleBackColor = true;
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(12, 162);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(230, 30);
            this.acceptButton.TabIndex = 4;
            this.acceptButton.Text = "Create the Scheme Line";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // referencePicUrlTextBox
            // 
            this.referencePicUrlTextBox.Location = new System.Drawing.Point(10, 110);
            this.referencePicUrlTextBox.Name = "referencePicUrlTextBox";
            this.referencePicUrlTextBox.Size = new System.Drawing.Size(230, 20);
            this.referencePicUrlTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Reference Pic URL";
            // 
            // SchemeExportForm
            // 
            this.AcceptButton = this.acceptButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 204);
            this.Controls.Add(this.referencePicUrlTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.riftCompatibleCheckBox);
            this.Controls.Add(this.authorNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.schemeNameTextBox);
            this.Controls.Add(this.label1);
            this.Name = "SchemeExportForm";
            this.Text = "Export Scheme as Scheme Line";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox schemeNameTextBox;
        private System.Windows.Forms.TextBox authorNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox riftCompatibleCheckBox;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.TextBox referencePicUrlTextBox;
        private System.Windows.Forms.Label label3;
    }
}