﻿using EDColorChanger.Core;
using System;
using System.Windows.Forms;

namespace EDColorChanger.GuiFront
{
    public partial class SchemeExportForm : Form
    {
        // The matrix to export
        private EdColorMatrix _matrix;

        /// <summary>
        /// Initializes a new instance of the <see cref="SchemeExportForm"/> class.
        /// </summary>
        /// <param name="matrix">The matrix to use.</param>
        public SchemeExportForm(EdColorMatrix matrix)
            : this()
        {
            // Get a copy so that we don't have to worry about changes propagating back
            _matrix = matrix.GetCopy();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SchemeExportForm"/> class.
        /// </summary>
        private SchemeExportForm()
        {
            InitializeComponent();
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            // Set the other attributes for the matrix
            _matrix.Name = schemeNameTextBox.Text;
            _matrix.AuthorName = authorNameTextBox.Text;
            _matrix.ReferencePicURL = referencePicUrlTextBox.Text;

            // If it's good for the rift, add that to the title
            if (riftCompatibleCheckBox.Checked)
            {
                _matrix.Name += "(Rift)";
            }

            // Get the scheme line
            var schemeLine = _matrix.GetSchemeLine();

            GuiHelper.SendTextToClipboard(schemeLine, "Scheme line copied to the clipboard. \n Here it is for reference:\n {0}");

            this.Close();
        }
    }
}