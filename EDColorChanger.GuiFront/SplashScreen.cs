﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace EDColorChanger.GuiFront
{
    public partial class SplashScreen : Form
    {
        // Basics taken from http://www.codeproject.com/Articles/5454/A-Pretty-Good-Splash-Screen-in-C

        private static SplashScreen _screen = null;
        private static Thread _thread = null;

        private double _opacityIncrement = .05;
        private double _opacityDecrement = .08;
        private const int TIMER_INTERVAL = 50;
        private const int SECONDS_SHOWN_MIN = 3;
        private Stopwatch _shownWatch = null;

        static public Thread ShowSplashScreen()
        {
            if (_screen != null)
                return _thread;

            _thread = new Thread(new ThreadStart(SplashScreen.ShowForm));
            _thread.IsBackground = true;
            _thread.SetApartmentState(ApartmentState.STA);
            _thread.Start();

            while (_screen == null || _screen.IsHandleCreated == false)
            {
                Thread.Sleep(TIMER_INTERVAL);
            }

            return _thread;
        }

        static public void ShowForm()
        {
            _screen = new SplashScreen();
            Application.Run(_screen);
        }

        static public void CloseForm()
        {
            if (_screen != null)
            {
                _screen._opacityIncrement = -_screen._opacityDecrement;
            }

            _thread = null;
            _screen = null;
        }

        private SplashScreen()
        {
            InitializeComponent();
            var transparentColor = Color.FromArgb(0, 255, 0); // Green
            TransparencyKey = transparentColor;
            BackColor = transparentColor;
            Opacity = 0;
            _shownWatch = new Stopwatch();
        }

        private void _updateTimer_Tick(object sender, EventArgs e)
        {
            if (!_shownWatch.IsRunning)
            {
                _shownWatch.Start();
            }
            else if (_shownWatch.ElapsedMilliseconds > SECONDS_SHOWN_MIN * 1000 && (_resetForm == null || !_resetForm.Visible))
            {
                CloseForm();
            }
            if (_opacityIncrement > 0)
            {
                if (Opacity < 1)
                {
                    Opacity += _opacityIncrement;
                }
            }
            else
            {
                if (Opacity > 0)
                {
                    Opacity += _opacityIncrement;
                }
                else if (Opacity <= 0.0)
                {
                    Close();
                }
            }
        }

        private void SplashScreen_DoubleClick(object sender, EventArgs e)
        {
            CloseForm();
        }

        private static ResetForm _resetForm = null;

        private void SplashScreen_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                _resetForm = new ResetForm();
                _resetForm.ShowDialog();
            }
        }
    }
}