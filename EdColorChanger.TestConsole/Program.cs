﻿using EDColorChanger.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EDColorChanger.TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const string filename = @"D:\Program Files (x86)\Frontier\EDLaunch\Products\FORC-FDEV-D-1002\GraphicsConfiguration.xml";

            var redString = "1, 2, 3";
            var greenString = "4, 5, 6";
            var blueString = "7, 8, 9";

            var m = EdColorMatrix.CreateFromMatrixString(redString,
                greenString,
                blueString);

            Console.WriteLine(m);
            Console.WriteLine();

            var matrices = new List<EdColorMatrix>();

            {
                var lines = File.ReadAllLines("./Schemes.txt").Where(l => !l.Trim().StartsWith("#"));
                foreach (var scheme in lines)
                {
                    matrices.Add(EdColorMatrix.BuildFromSchemeLine(scheme));
                }
            }

            foreach (var colorMatrix in matrices)
            {
                Console.WriteLine(colorMatrix);
                Console.WriteLine();
            }

            EDColorChanger.Core.ColorChanger.ChangeColor(filename, matrices[0], "graphics.backup", true);

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}